package clock.socoolby.com.clock.db;

import org.junit.Before;
import org.junit.Test;

import e.odbo.data.E;
import e.odbo.data.model.Table;
import e.odbo.data.util.TableUtils;

import static org.junit.Assert.*;

public class WorkClockDatabaseTest {

    WorkClockDatabase database;

    @Before
    public void setUp() throws Exception {
        database=new WorkClockDatabase();
    }

    @Test
    public void testTableDAO() throws Exception{
        printDao(database.themeUI);
    }


    public void printDao(Table table){
        TableUtils.printTableDAO(table);
        TableUtils.printTableBean(table);
    }
}