package com.xenione.digit;

public  class TabConfig{
    private Boolean charBackgroundBorder;

    private Integer charBackgroundBorderColor;

    private Integer charBackgroundBorderDividerColor;

    private Integer charBackgroundBorderDividerWidth;

    private float mElapsedTime;

    public TabConfig(Boolean charBackgroundBorder, Integer charBackgroundBorderColor, Integer charBackgroundBorderDividerColor, Integer charBackgroundBorderDividerWidth,float mElapsedTime) {
        this.charBackgroundBorder = charBackgroundBorder;
        this.charBackgroundBorderColor = charBackgroundBorderColor;
        this.charBackgroundBorderDividerColor = charBackgroundBorderDividerColor;
        this.charBackgroundBorderDividerWidth = charBackgroundBorderDividerWidth;
        this.mElapsedTime=mElapsedTime;
    }

    public Boolean getCharBackgroundBorder() {
        return charBackgroundBorder;
    }

    public void setCharBackgroundBorder(Boolean charBackgroundBorder) {
        this.charBackgroundBorder = charBackgroundBorder;
    }

    public Integer getCharBackgroundBorderColor() {
        return charBackgroundBorderColor;
    }

    public void setCharBackgroundBorderColor(Integer charBackgroundBorderColor) {
        this.charBackgroundBorderColor = charBackgroundBorderColor;
    }

    public Integer getCharBackgroundBorderDividerColor() {
        return charBackgroundBorderDividerColor;
    }

    public void setCharBackgroundBorderDividerColor(Integer charBackgroundBorderDividerColor) {
        this.charBackgroundBorderDividerColor = charBackgroundBorderDividerColor;
    }

    public Integer getCharBackgroundBorderDividerWidth() {
        return charBackgroundBorderDividerWidth;
    }

    public void setCharBackgroundBorderDividerWidth(Integer charBackgroundBorderDividerWidth) {
        this.charBackgroundBorderDividerWidth = charBackgroundBorderDividerWidth;
    }

    public float getmElapsedTime() {
        return mElapsedTime;
    }

    public void setmElapsedTime(float mElapsedTime) {
        this.mElapsedTime = mElapsedTime;
    }
}