package clock.socoolby.com.clock.dao.base;
import e.odbo.data.dao.BaseDAO;
import e.odbo.data.dao.table.TableDefinition;
import e.odbo.data.dao.table.Field;

import com.openbravo.data.basic.BasicException;
import com.openbravo.data.loader.I_Session;
import com.openbravo.data.loader.serialize.Datas;
import e.odbo.data.format.Formats;
import com.openbravo.data.loader.serialize.DataWrite;
import com.openbravo.data.loader.serialize.DataRead;


public class TimeFontStyleDao extends BaseDAO<TimeFontStyle> {

    public TimeFontStyleDao(I_Session s) {
        super(s);
    }

    @Override
    public Class getSuportClass() {
        return TimeFontStyle.class;
    }

// tableDefinition table:	time_font_style 	build time :2019-06-16 15:01:08.154

    @Override
    public TableDefinition getTable() {
        TableDefinition time_font_styleTableDefinition=new TableDefinition("time_font_style" ,new Field[]{
                new Field(TimeFontStyle.NAME,Datas.STRING,Formats.STRING),//(PK)
                new Field(TimeFontStyle.DISPLAY_SECOND,Datas.INT,Formats.INT),//(notNull)
                new Field(TimeFontStyle.NO_DISPLAY_SECOND,Datas.INT,Formats.INT),//(notNull)
                new Field(TimeFontStyle.DISPLAY_SECOND_ON_FULL,Datas.INT,Formats.INT),//(notNull)
                new Field(TimeFontStyle.NO_DISPLAY_SECOND_ON_FULL,Datas.INT,Formats.INT)//(notNull)
        }, new int[] {0}
        );
        return time_font_styleTableDefinition;
    }

    @Override
    public void writeInsertValues(DataWrite dp,TimeFontStyle obj) throws BasicException {
        dp.setString(1,obj.getName());
        dp.setInt(2,obj.getDisplaySecond());
        dp.setInt(3,obj.getNoDisplaySecond());
        dp.setInt(4,obj.getDisplaySecondOnFull());
        dp.setInt(5,obj.getNoDisplaySecondOnFull());
    }


    public TimeFontStyle readValues(DataRead dr,TimeFontStyle obj) throws BasicException {
        if(obj==null)
            obj = new TimeFontStyle();
        obj.setName(dr.getString(1));
        obj.setDisplaySecond(dr.getInt(2));
        obj.setNoDisplaySecond(dr.getInt(3));
        obj.setDisplaySecondOnFull(dr.getInt(4));
        obj.setNoDisplaySecondOnFull(dr.getInt(5));
        return obj;
    }
}
