package clock.socoolby.com.clock.widget.animatorview.animator.clockanimator;

import android.graphics.Canvas;
import android.graphics.Paint;

public interface I_Pointer {

    void init(float mRadius,int mClockColor);

    void drawPointer(Canvas canvas, int mH,int mM,int mS, Paint mPointerPaint);

    String typeName();

    void setmPointerColor(int mClockColor);

    void setColorSecond(int colorSecond);
}
