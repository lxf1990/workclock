package clock.socoolby.com.clock.alter;

import clock.socoolby.com.clock.model.DateModel;
import clock.socoolby.com.clock.viewmodel.GlobalViewModel;
import e.odbo.data.dao.EntityManager;

public class AlterManager {
    DateModel startTime;
    DateModel stopTime;

    GlobalViewModel globalViewModel;

    EntityManager entityManager;

    public AlterManager(DateModel startTime, DateModel stopTime,GlobalViewModel globalViewModel,EntityManager entityManager) {
        this.startTime = startTime;
        this.stopTime = stopTime;
        this.globalViewModel=globalViewModel;
        this.entityManager=entityManager;
    }

    public boolean isReport(int hour, int minute) {
        DateModel nowTime = new DateModel();
        nowTime.setTime(hour, minute);

        if (startTime.getShortTimeString().equals(stopTime.getShortTimeString()))
            return true;
        long minutes = startTime.minusTime(stopTime);
        if (minutes < 0) {//stop>start
            if (nowTime.minusTime(startTime) >= 0 && nowTime.minusTime(stopTime) <= 0) {
                return false;
            }
        }
        return true;
    }

}
