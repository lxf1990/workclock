package clock.socoolby.com.clock.widget.textview.border;

import android.graphics.Canvas;
import android.graphics.Paint;

import clock.socoolby.com.clock.widget.textview.I_Border;

public abstract class AbstractBorder implements I_Border {

    public void drawDivider(Canvas canvas, float startX, float startY, int width, int height, Paint mDividerPaint) {
        canvas.drawLine(startX+mDividerPaint.getStrokeWidth(), startY+height/2, startX+width-mDividerPaint.getStrokeWidth(), startY+height/2, mDividerPaint);
    }
}
