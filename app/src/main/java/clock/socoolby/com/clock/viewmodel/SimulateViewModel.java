package clock.socoolby.com.clock.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import clock.socoolby.com.clock.model.SharePerferenceModel;
import clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.SimulateTextShowTypeEnum;

public class SimulateViewModel extends ViewModel {

    private MutableLiveData<String> clockTypeName=new MutableLiveData<>();

    private MutableLiveData<String> pointerTypeName=new MutableLiveData<>();

    private MutableLiveData<Integer> simulateClockColorScale=new MutableLiveData<>();
    private MutableLiveData<Integer> simulateClockColorScaleParticularly=new MutableLiveData<>();
    private MutableLiveData<Integer> simulateClockColorText=new MutableLiveData<>();
    private MutableLiveData<Integer> simulateClockColorOutLine=new MutableLiveData<>();

    private MutableLiveData<Integer> simulateClockColorPointer=new MutableLiveData<>();

    private MutableLiveData<Integer> simulateClockColorPointerSecond=new MutableLiveData<>();

    private MutableLiveData<SimulateTextShowTypeEnum> simulateClockTextShowType=new MutableLiveData<>();


    SharePerferenceModel model;

    public SimulateViewModel(SharePerferenceModel model) {
        this.model = model;
        loadFromModel();
    }

    public void loadFromModel(){
        clockTypeName.setValue(model.getSimulateClockTypeName());
        pointerTypeName.setValue(model.getSimulateClockPointerTypeName());

        simulateClockColorScale.setValue(model.getSimulateClockColorScale());
        simulateClockColorScaleParticularly.setValue(model.getSimulateClockColorScaleParticularly());
        simulateClockColorText.setValue(model.getSimulateClockColorText());
        simulateClockColorOutLine.setValue(model.getSimulateClockColorOutLine());
        simulateClockTextShowType.setValue(SimulateTextShowTypeEnum.valueOf(model.getSimulateClockTextShowType()));
        simulateClockColorPointer.setValue(model.getSimulateClockColorPointer());
        simulateClockColorPointerSecond.setValue(model.getSimulateClockColorPointerSecond());
    }

    public MutableLiveData<String> getClockTypeName() {
        return clockTypeName;
    }

    public MutableLiveData<String> getPointerTypeName() {
        return pointerTypeName;
    }

    public void setClockTypeName(String clockTypeName) {
        this.clockTypeName.setValue(clockTypeName);
        model.setSimulateClockTypeName(clockTypeName);
    }

    public void setPointerTypeName(String pointerTypeName) {
        this.pointerTypeName.setValue(pointerTypeName);
        model.setSimulateClockPointerTypeName(pointerTypeName);
    }

    public MutableLiveData<Integer> getSimulateClockColorScale() {
        return simulateClockColorScale;
    }

    public void setSimulateClockColorScale(Integer simulateClockColorScale) {
        this.simulateClockColorScale.setValue(simulateClockColorScale);
        model.setSimulateClockColorScale(simulateClockColorScale);
    }

    public MutableLiveData<Integer> getSimulateClockColorScaleParticularly() {
        return simulateClockColorScaleParticularly;
    }

    public void setSimulateClockColorScaleParticularly(Integer simulateClockColorScaleParticularly) {
        this.simulateClockColorScaleParticularly.setValue(simulateClockColorScaleParticularly);
        model.setSimulateClockColorScaleParticularly(simulateClockColorScaleParticularly);
    }

    public MutableLiveData<Integer> getSimulateClockColorText() {
        return simulateClockColorText;
    }

    public void setSimulateClockColorText(Integer simulateClockColorText) {
        this.simulateClockColorText.setValue(simulateClockColorText);
        model.setSimulateClockColorText(simulateClockColorText);
    }

    public MutableLiveData<Integer> getSimulateClockColorOutLine() {
        return simulateClockColorOutLine;
    }

    public void setSimulateClockColorOutLine(Integer simulateClockColorOutLine) {
        this.simulateClockColorOutLine.setValue(simulateClockColorOutLine);
        model.setSimulateClockColorOutLine(simulateClockColorOutLine);
    }

    public MutableLiveData<SimulateTextShowTypeEnum> getSimulateClockTextShowType() {
        return simulateClockTextShowType;
    }

    public void setSimulateClockTextShowType(SimulateTextShowTypeEnum simulateClockTextShowType) {
        this.simulateClockTextShowType.setValue(simulateClockTextShowType);
        model.setSimulateClockTextShowType(simulateClockTextShowType.code);
    }


    public MutableLiveData<Integer> getSimulateClockColorPointer() {
        return simulateClockColorPointer;
    }

    public void setSimulateClockColorPointer(Integer simulateClockColorPointer) {
        this.simulateClockColorPointer.setValue(simulateClockColorPointer);
        model.setSimulateClockColorPointer(simulateClockColorPointer);
    }

    public MutableLiveData<Integer> getSimulateClockColorPointerSecond() {
        return simulateClockColorPointerSecond;
    }

    public void setSimulateClockColorPointerSecond(Integer simulateClockColorPointerSecond) {
        this.simulateClockColorPointerSecond.setValue(simulateClockColorPointerSecond);
        model.setSimulateClockColorPointerSecond(simulateClockColorPointerSecond);
    }
}
