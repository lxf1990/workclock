package clock.socoolby.com.clock.todo.microsoft.bean.base;

import org.json.JSONException;
import org.json.JSONObject;

import clock.socoolby.com.clock.net.base.I_JsonObject;

/**
 * 内容	String	项目的内容。
 * AttachmentContentType	String	内容的类型。可能的值为 text 和 HTML。
 */

public class ItemBody implements I_JsonObject {
    String content;
    String contentType;

    @Override
    public void fromJson(JSONObject jsonObject) throws JSONException {
        content=  jsonObject.getString("content");
        contentType=jsonObject.getString("contentType");
    }

    @Override
    public void toJson(JSONObject jsonObject) throws JSONException {

    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}
