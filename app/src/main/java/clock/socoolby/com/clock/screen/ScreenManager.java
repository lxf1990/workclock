package clock.socoolby.com.clock.screen;

import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PowerManager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;

import java.util.List;

import clock.socoolby.com.clock.ActivateAdmin;
import clock.socoolby.com.clock.ClockApplication;
import timber.log.Timber;

public class ScreenManager {
    public static boolean isScreenOn() {
        PowerManager pm = (PowerManager) ClockApplication.getContext().getSystemService(AppCompatActivity.POWER_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
           return pm.isInteractive();
        }
        return pm.isScreenOn();
    }

    public static void systemLock(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            AccessibilityManager manager = (AccessibilityManager) context.getSystemService(Context.ACCESSIBILITY_SERVICE);
            if (manager.isEnabled()) {
                AccessibilityEvent event = AccessibilityEvent.obtain(AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED);
                event.setPackageName(context.getPackageName());
                event.setClassName(ScreenManager.class.getName());
                event.setEnabled(true);
                event.getText().add(LockAccessibilityService.LOCK_COMMAND);
                Timber.v("Accessibility enabled. Firing AccessibilityEvent: $event");
                manager.sendAccessibilityEvent(event);
            } else {
                Timber.v("Accessibility disabled. Can't fire AccessibilityEvent");
            }
            return;
        }
        DevicePolicyManager policyManager = (DevicePolicyManager) context.getSystemService(AppCompatActivity.DEVICE_POLICY_SERVICE);
        ComponentName componentName= new ComponentName(context, ActivateAdmin.class);

        if (!policyManager.isAdminActive(componentName)) {
            Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
            intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, componentName);
            //描述(additional explanation)
            intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "------ 用于锁定屏幕 ------");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
            context.startActivity(intent);
        }
        if (policyManager.isAdminActive(componentName)) {
            policyManager.lockNow();
        }

    }

    public static void systemUnLock() {
        PowerManager pm = (PowerManager) ClockApplication.getContext().getSystemService(AppCompatActivity.POWER_SERVICE);
        PowerManager.WakeLock mWakelock = pm.newWakeLock(PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "workclock:SimpleTimer");
        mWakelock.acquire();
        mWakelock.release();
        KeyguardManager keyguardManager = (KeyguardManager)  ClockApplication.getContext().getSystemService(AppCompatActivity.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("unLock");
        keyguardLock.disableKeyguard(); // 解锁屏
    }

    public static boolean isApplicationBroughtToBackground(final Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return true;
            }
        }
        return false;
    }

}
