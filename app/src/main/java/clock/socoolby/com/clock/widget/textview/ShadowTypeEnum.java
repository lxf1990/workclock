package clock.socoolby.com.clock.widget.textview;

public enum ShadowTypeEnum {
    NOSETUP(0),SHADOW(1),RELIEF(2);

    int typeCode;

     ShadowTypeEnum(int typeCode) {
        this.typeCode = typeCode;
    }

    public static ShadowTypeEnum valueOf(int type){
         switch (type){
             case 1:
                 return SHADOW;
             case 2:
                 return RELIEF;
             default:
                  return NOSETUP;
         }
    }

    public int getTypeCode() {
        return typeCode;
    }
}
