package clock.socoolby.com.clock.todo.microsoft.bean.todo;

public enum TodoStatusEnum {
    notStarted,inProgress,completed,waitingOnOthers,deferred
}
