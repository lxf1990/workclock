package clock.socoolby.com.clock.widget.animatorview.animator.clockanimator;

import android.graphics.Canvas;
import android.graphics.Paint;

//六角形
public class HexagonalClock extends AbstractClock {


    /**
     * 绘制时钟的圆形和刻度
     */
    protected void drawBorder(Canvas canvas) {
        canvas.save();
        canvas.translate(mCenterX, mCenterY);

        float radianTanValue=0f;
        int j=1;
        float stepLeng=0;
        float startX=0;
        float startY=-mRadius;
        float scaleLength=0;
        float floatLength=0;
        float octagonalLineLenght=new Double(Math.tan(Math.toRadians(30))*mRadius).floatValue()*2;
        float lineStartX=(0-octagonalLineLenght)/2;

        for(int i=0;i<6;i++) {
            j=1;
            stepLeng=0;

            //边框，好像不画好看
            //mDefaultPaint.setStrokeWidth(mDefaultScaleWidth);
            //mDefaultPaint.setParticleColor(mClockColor);
            //canvas.drawLine(lineStartX, startY, lineStartX+octagonalLineLenght,startY, mDefaultPaint);

            mDefaultPaint.setStrokeWidth(mParticularlyScaleWidth);
            mDefaultPaint.setColor(mColorParticularyScale);
            canvas.drawLine(startX, startY, startX, startY + mParticularlyScaleLength, mDefaultPaint);
            while (stepLeng < octagonalLineLenght / 2) {//上下面
                radianTanValue = new Double(Math.tan(Math.toRadians(6 * j))).floatValue();
                stepLeng = radianTanValue * mRadius;

                if (j % 5 == 0) { // 特殊时刻
                    mDefaultPaint.setStrokeWidth(mParticularlyScaleWidth);
                    mDefaultPaint.setColor(mColorParticularyScale);
                    mDefaultPaint.setAlpha(255);
                    scaleLength = mParticularlyScaleLength;
                } else {          // 一般时刻
                    mDefaultPaint.setStrokeWidth(mDefaultScaleWidth);
                    mDefaultPaint.setColor(mColorDefaultScale);
                    mDefaultPaint.setAlpha(100);
                    scaleLength = mDefaultScaleLength;
                }
                floatLength = radianTanValue * scaleLength;
                canvas.drawLine(startX + stepLeng, startY, startX + stepLeng - floatLength, startY + scaleLength, mDefaultPaint);
                canvas.drawLine(startX - stepLeng, startY, startX - stepLeng + floatLength, startY + scaleLength, mDefaultPaint);
                j++;
            }
            canvas.rotate(60);
        }
        canvas.restore();
    }

    @Override
    public  String typeName() {
        return TYPE_HEXAGONAL;
    }

    public static final String TYPE_HEXAGONAL="hexagonal";

}
