package clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.pointer;

import android.graphics.Canvas;

public class TwoStepPointer extends SecondTailPointer{

    /**
     * 绘制时针
     */
    protected void drawHourPointer(Canvas canvas) {

        mPointerPaint.setStrokeWidth(mHourPointerWidth*2);
        mPointerPaint.setColor(mColorHourPointer);

        // 当前时间的总秒数
        float s = mH * 60 * 60 + mM * 60 + mS;
        // 百分比
        float percentage = s / (12 * 60 * 60);
        // 通过角度计算弧度值，因为时钟的角度起线是y轴负方向，而View角度的起线是x轴正方向，所以要加270度
        float angle = calcAngle(percentage);

        float x = calcX(mHourPointerLength , angle);
        float y = calcY(mHourPointerLength , angle);

        float x1 = calcX(mHourPointerLength/4 , angle);
        float y1 = calcY(mHourPointerLength/4 , angle);

        canvas.drawLine(x1, y1, x, y, mPointerPaint);

        mPointerPaint.setStrokeWidth(mHourPointerWidth/4);
        canvas.drawLine(0,0,x1, y1,  mPointerPaint);
    }

    /**
     * 绘制分针
     */
    protected void drawMinutePointer(Canvas canvas) {
        mPointerPaint.setStrokeWidth(mMinutePointerWidth*2);
        mPointerPaint.setColor(mColorMinutePointer);

        float s = mM * 60 + mS;
        float percentage = s / (60 * 60);
        float angle = calcAngle(percentage);

        float x = calcX(mMinutePointerLength , angle);
        float y = calcY(mMinutePointerLength , angle);

        float x1 = calcX(mMinutePointerLength/4 , angle);
        float y1 = calcY(mMinutePointerLength/4 , angle);

        canvas.drawLine(x1, y1, x, y, mPointerPaint);

        mPointerPaint.setStrokeWidth(mMinutePointerWidth/4);
        canvas.drawLine(0,0,x1, y1,  mPointerPaint);
    }

    public static final String TYPE_TWO_STEP_POINTER="twoStepPointer";


    @Override
    public String typeName() {
        return TYPE_TWO_STEP_POINTER;
    }
}
