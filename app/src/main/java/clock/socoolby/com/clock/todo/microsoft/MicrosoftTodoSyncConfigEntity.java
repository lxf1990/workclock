package clock.socoolby.com.clock.todo.microsoft;

import org.json.JSONException;
import org.json.JSONObject;

import clock.socoolby.com.clock.todo.config.AbstractTodoSyncConfigEntity;

public class MicrosoftTodoSyncConfigEntity extends AbstractTodoSyncConfigEntity {
    String folderId="";
    String groupId="";

    //TodoStatusEnum ableStatus;

    @Override
    public void fromJson(JSONObject jsonObject) throws JSONException {
             super.fromJson(jsonObject);
             folderId=jsonObject.optString("folderId","");
             groupId=jsonObject.optString("groupId","");
    }

    @Override
    protected String getServiceName() {
        return MicrosoftTodoSyncServiceImpl.NAME;
    }

    @Override
    public void toJson(JSONObject jsonObject) throws JSONException {
             super.toJson(jsonObject);
             jsonObject.put("groupId",groupId);
             jsonObject.put("folderId",folderId);
    }

    public String getFolderId() {
        return folderId;
    }

    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }
}
