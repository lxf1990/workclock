package clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.pointer;

import android.graphics.Canvas;
import android.graphics.Color;

public class SecondTailPointer extends AbstractPointer{

    @Override
    public void initPointerLength(float mRadius) {
        super.initPointerLength(mRadius);
        mHourPointerLength = mRadius / 8*4.5f;
        mMinutePointerLength = mRadius / 8*6;
        mSecondPointerLength = mRadius / 8 * 7;
    }

    @Override
    protected void drawCenterCircle(Canvas canvas) {
        super.drawCenterCircle(canvas);
        mPointerPaint.setColor(Color.BLACK);
        canvas.drawCircle(0, 0, mPointRadius/2, mPointerPaint);
    }

    @Override
    protected void drawSecondPointer(Canvas canvas) {
        super.drawSecondPointer(canvas);

        mPointerPaint.setStrokeWidth(mSecondPointerWidth*1.5f);

        float s = mS;
        float percentage = s / 60;
        float angle = calcAngle(percentage)+180;

        float x = calcX(mSecondPointerLength/5 , angle);
        float y = calcY(mSecondPointerLength/5 , angle);

        canvas.drawLine(0, 0, x, y, mPointerPaint);

        canvas.drawCircle(0,0,mSecondPointerWidth*2,mPointerPaint);
    }

    public static final String TYPE_SECOND_TAIL="secondTail";


    @Override
    public String typeName() {
        return TYPE_SECOND_TAIL;
    }
}
