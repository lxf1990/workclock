package clock.socoolby.com.clock.model;

import android.graphics.Color;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import clock.socoolby.com.clock.Constants;

public class SharePerferenceModel extends AbstractPerferenceModel {

    public static final int DEFAULT_COLOR=Color.rgb(255, 255, 255);

    protected boolean firstStart = true;
    protected final static String KEY_FIRST_START = "key_first_start";

    protected int typeHourPower = Constants.TALKING_HALF_AN_HOUR;
    protected final static String KEY_TYPE_HOUR_POWER = "key_type_hour_power";

    protected final static String KEY_SCREEN_LOCK_PASSWORD="key_screen_lock_password";
    protected Integer screenLockPassword=0;

    protected DateModel startHourPowerTime = null;
    protected final static String KEY_START_POWER = "key_start_power";
    protected DateModel stopHourPowerTime = null;
    protected final static String KEY_STOP_POWER = "key_stop_power";


    protected boolean tickSound = false;
    protected final static String KEY_IS_TICK_SOUND = "key_is_tick_sound";
    protected boolean triggerScreen = false;
    protected final static String KEY_IS_TRIGGER_SCREEN = "key_is_trigger_screen";

    protected boolean hourSystem12 = false;
    protected final static String KEY_IS_HOUR_SYSTEM_12 = "key_is_hour_system_12";

    protected final static String KEY_TIME_HOUR_ALTER_TYPE_NAME= "key_time_hour_alter_type_name";
    protected String timeHourAlterTypeName="default";

    protected final static String KEY_TIME_HOUR_ALTER_DIALY= "key_time_hour_alter_dialy";
    protected Integer timeHourAlterDialy=20;

    protected final static String KEY_TIME_HOUR_VIDEO_PATH= "key_time_hour_video_path";
    protected String timeHourVideoPath="";


    protected final static String KEY_CITY = "key_city";
    protected String mCity="jinhua";

    protected final static String KEY_DESCRPTION = "key_description";
    protected String mDescription="";


    protected final static String KEY_HANDUP_TIME="key_handup_time";
    protected Integer handUpTime=-1;

    protected  final static String KEY_IS_HANDUP_ABLE="key_is_handup_able";
    protected boolean handUpAble = false;

    protected  final static String KEY_HANDUP_MUSIC ="key_handup_music";
    protected String handUpMusic = "";

    protected  final static String KEY_HANDUP_MUSIC_NAME ="key_handup_music_name";
    protected String handUpMusicName = "default";

    protected final static String KEY_HANDUP_DIALY="key_handup_dialy";
    protected Integer handUpDialy=60;

    protected final static String KEY_HANDUP_OVERTIME_COUNTING_ABLE="key_handup_overtime_counting_able";
    protected Boolean handUpOverTimeCountingAble=true;

    protected  final static String KEY_HANDUP_TYPE_NAME ="key_handup_type_name";
    protected String handUpTypeName = "default";


    protected final static String KEY_BACKGROUND_COLOR="key_background_color";
    protected Integer backgroundColor=Color.rgb(0, 0, 0);

    protected final static String KEY_BACKGROUND_IMAGE="key_background_image";
    protected String backgroundImage="";


    protected final static String KEY_BACKGROUND_ANIMATOR="key_background_animator";
    protected String backgroundAnimatorName="";

    protected final static String KEY_BACKGROUND_ANIMATOR_RANDOM_COLOR="key_background_animator_random_color";
    protected boolean backgroundAnimatorRandomColor=false;

    protected final static String KEY_BACKGROUND_IMAGE_VISABLE="key_background_image_visable";
    protected boolean backgroundImageVisable=false;


    protected final static String KEY_USE_SYSTEM_WALLPAPER="key_use_system_wallpaper";
    protected boolean useSystemWallpaper=false;


    protected final static String KEY_FOREGROUND_COLOR="key_foreground_color";
    protected Integer foregroundColor=DEFAULT_COLOR;

    protected final static String KEY_FOREGROUND_COLOR1="key_foreground_color1";
    protected Integer foregroundColor1=Color.rgb(199,21,133);

    protected final static String KEY_FOREGROUND_COLOR2="key_foreground_color2";
    protected Integer foregroundColor2=DEFAULT_COLOR;

    //protected final static String KEY_FONT_INDEX = "key_font_index";
    //protected Integer fontIndex;

    //数字时钟 time text style
    DigitPerferenceModel digitPerferenceModel=new DigitPerferenceModel();

    //模拟时钟
    SimulatePerferenceModel simulatePerferenceModel=new SimulatePerferenceModel();

    //ui
    protected final static String KEY_UI_CLOCK_INTERFACE_TYPE ="key_ui_clock_interface_type";
    protected int clockInterfaceType=0;

    protected final static String KEY_UI_AUTO_FULLSCREEN ="key_ui_auto_fullscreen";
    protected boolean autoFullscreen=false;

    protected final static String KEY_UI_FULLSCREEN_SPIRIT_ABLE ="key_ui_fullscreen_spirit_able";
    protected boolean fullscreenSpiritAble=false;

    //theme ui
    protected final static String KEY_THEME_UI_NAME="key_theme_ui_name";
    protected String themeName="default";

    protected final static String KEY_THEME_UI_TYPE="key_theme_ui_type";
    protected int themeUIType=1;

    protected final static String KEY_SYS_BOOT_START="key_sys_boot_start";
    protected boolean bootStart=false;

    //sync
    protected final static String KEY_SYS_TODO_SYNC_ABLE="key_sys_todo_sync_able";
    protected boolean todoSyncAble=false;

    private int dirtyDelaySecond=10000;

    private boolean dirtySaving=false;

    Timer timer=null;

    private void dirtySave(){
        Log.d("model","comming in dirty save task...");
        if(dirtySaving)
             return;
        dirtySaving=true;
        if(timer==null)
            timer = new Timer();
        Log.d("model","dirty task created ...");
        timer.schedule(new TimerTask() {
            public void run() {
                Log.d("model","dirty task running...");
                save();
                dirtySaving=false;
                this.cancel();
            }
        }, dirtyDelaySecond);
    }

    public int getTypeHourPower() {
        return typeHourPower;
    }

    public void setTypeHourPower(int typeHourPower) {
        this.typeHourPower = typeHourPower;
        dirtySave();
    }

    public DateModel getStartHourPowerTime() {
        return startHourPowerTime;
    }


    public void setStartHourPowerTime(DateModel startHourPowerTime) {
        this.startHourPowerTime = startHourPowerTime;
        dirtySave();
    }

    public DateModel getStopHourPowerTime() {
        return stopHourPowerTime;
    }

    public void setStopHourPowerTime(DateModel stopHourPowerTime) {
        this.stopHourPowerTime = stopHourPowerTime;
        dirtySave();
    }

    public boolean isDisplaySecond() {
        return digitPerferenceModel.displaySecond;
    }

    public void setDisplaySecond(boolean displaySecond) {
        digitPerferenceModel.displaySecond = displaySecond;
        dirtySave();
    }

    public boolean isTickSound() {
        return tickSound;
    }

    public void setTickSound(boolean tickSound) {
        this.tickSound = tickSound;
        dirtySave();
    }

    public boolean isTriggerScreen() {
        return triggerScreen;
    }

    public void setTriggerScreen(boolean triggerScreen) {
        this.triggerScreen = triggerScreen;
        dirtySave();
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        this.mCity = city;
        dirtySave();
    }

    public String getDescription() {
        if(mDescription.isEmpty())
            return Constants.APP_MEMO;
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
        dirtySave();
    }


    @Override
    public void fromJson(JSONObject jsonObject) throws JSONException {
        firstStart=jsonObject.optBoolean(KEY_FIRST_START,true);
        typeHourPower = jsonObject.getInt(KEY_TYPE_HOUR_POWER);

        screenLockPassword=jsonObject.optInt(KEY_SCREEN_LOCK_PASSWORD,0);

        tickSound = jsonObject.getBoolean(KEY_IS_TICK_SOUND);
        triggerScreen =jsonObject.optBoolean(KEY_IS_TRIGGER_SCREEN,false);
        mCity = jsonObject.optString(KEY_CITY,"jinhua");
        mDescription = jsonObject.optString(KEY_DESCRPTION, "");
        startHourPowerTime = new DateModel();
        startHourPowerTime.setDataString(jsonObject.getString(KEY_START_POWER));
        stopHourPowerTime = new DateModel();
        stopHourPowerTime.setDataString(jsonObject.getString(KEY_STOP_POWER));

        timeHourAlterTypeName=jsonObject.optString(KEY_TIME_HOUR_ALTER_TYPE_NAME,"default");
        timeHourAlterDialy=jsonObject.optInt(KEY_TIME_HOUR_ALTER_DIALY,20);

        timeHourVideoPath=jsonObject.optString(KEY_TIME_HOUR_VIDEO_PATH,"");

        handUpAble=jsonObject.optBoolean(KEY_IS_HANDUP_ABLE,false);
        handUpTime=jsonObject.optInt(KEY_HANDUP_TIME,-1);
        handUpMusic=jsonObject.optString(KEY_HANDUP_MUSIC,"");
        handUpDialy=jsonObject.optInt(KEY_HANDUP_DIALY,60);
        handUpTypeName=jsonObject.optString(KEY_HANDUP_TYPE_NAME,"default");
        handUpMusicName=jsonObject.optString(KEY_HANDUP_MUSIC_NAME,"default");
        handUpOverTimeCountingAble=jsonObject.optBoolean(KEY_HANDUP_OVERTIME_COUNTING_ABLE,true);


        backgroundImage=jsonObject.optString(KEY_BACKGROUND_IMAGE,"");
        backgroundAnimatorRandomColor=jsonObject.optBoolean(KEY_BACKGROUND_ANIMATOR_RANDOM_COLOR,false);
        backgroundImageVisable=jsonObject.optBoolean(KEY_BACKGROUND_IMAGE_VISABLE,false);


        foregroundColor1=jsonObject.optInt(KEY_FOREGROUND_COLOR1,Color.rgb(199,21,133));

        foregroundColor2=jsonObject.optInt(KEY_FOREGROUND_COLOR2,DEFAULT_COLOR);

        hourSystem12=jsonObject.optBoolean(KEY_IS_HOUR_SYSTEM_12,false);

        digitPerferenceModel.fromJsonString(jsonObject);

        simulatePerferenceModel.fromJsonString(jsonObject);

        clockInterfaceType =jsonObject.optInt(KEY_UI_CLOCK_INTERFACE_TYPE,0);
        autoFullscreen=jsonObject.optBoolean(KEY_UI_AUTO_FULLSCREEN,false);
        fullscreenSpiritAble=jsonObject.optBoolean(KEY_UI_FULLSCREEN_SPIRIT_ABLE,false);

        bootStart=jsonObject.optBoolean(KEY_SYS_BOOT_START,false);

        todoSyncAble=jsonObject.optBoolean(KEY_SYS_TODO_SYNC_ABLE,false);

        useSystemWallpaper=jsonObject.optBoolean(KEY_USE_SYSTEM_WALLPAPER,false);

        themeUILoad(jsonObject);
    }

    @Override
    public void toJson(JSONObject jsonObject) throws JSONException {
        jsonObject.put(KEY_FIRST_START,firstStart);
        jsonObject.put(KEY_TYPE_HOUR_POWER, typeHourPower);

        jsonObject.put(KEY_SCREEN_LOCK_PASSWORD,screenLockPassword);

        jsonObject.put(KEY_IS_TICK_SOUND, tickSound);
        jsonObject.put(KEY_IS_TRIGGER_SCREEN, triggerScreen);
        jsonObject.put(KEY_CITY, mCity);
        jsonObject.put(KEY_DESCRPTION, mDescription);
        jsonObject.put(KEY_START_POWER, startHourPowerTime.getTime());
        jsonObject.put(KEY_STOP_POWER, stopHourPowerTime.getTime());

        jsonObject.put(KEY_HANDUP_TIME,handUpTime);
        jsonObject.put(KEY_HANDUP_DIALY,handUpDialy);
        jsonObject.put(KEY_HANDUP_TYPE_NAME,handUpTypeName);

        jsonObject.put(KEY_IS_HANDUP_ABLE,handUpAble);
        jsonObject.put(KEY_HANDUP_MUSIC,handUpMusic);
        jsonObject.put(KEY_HANDUP_MUSIC_NAME,handUpMusicName);
        jsonObject.put(KEY_HANDUP_OVERTIME_COUNTING_ABLE,handUpOverTimeCountingAble);

        jsonObject.put(KEY_BACKGROUND_IMAGE,backgroundImage);
        jsonObject.put(KEY_BACKGROUND_IMAGE_VISABLE,backgroundImageVisable);

        jsonObject.put(KEY_BACKGROUND_ANIMATOR_RANDOM_COLOR,backgroundAnimatorRandomColor);

        jsonObject.put(KEY_FOREGROUND_COLOR1,foregroundColor1);
        jsonObject.put(KEY_FOREGROUND_COLOR2,foregroundColor2);

        jsonObject.put(KEY_IS_HOUR_SYSTEM_12,hourSystem12);

        jsonObject.put(KEY_TIME_HOUR_ALTER_TYPE_NAME,timeHourAlterTypeName);
        jsonObject.put(KEY_TIME_HOUR_ALTER_DIALY,timeHourAlterDialy);
        jsonObject.put(KEY_TIME_HOUR_VIDEO_PATH,timeHourVideoPath);

        digitPerferenceModel.toJsonString(jsonObject);

        simulatePerferenceModel.toJsonString(jsonObject);

        jsonObject.put(KEY_UI_CLOCK_INTERFACE_TYPE, clockInterfaceType);

        jsonObject.put(KEY_UI_AUTO_FULLSCREEN,autoFullscreen);
        jsonObject.put(KEY_UI_FULLSCREEN_SPIRIT_ABLE,fullscreenSpiritAble);

        jsonObject.put(KEY_SYS_BOOT_START,bootStart);

        jsonObject.put(KEY_SYS_TODO_SYNC_ABLE,todoSyncAble);

        jsonObject.put(KEY_USE_SYSTEM_WALLPAPER,useSystemWallpaper);

        themeUISave(jsonObject);
    }

    public void themeUISave(JSONObject jsonObject) throws JSONException {
        jsonObject.put(KEY_BACKGROUND_COLOR,backgroundColor);
        jsonObject.put(KEY_BACKGROUND_ANIMATOR,backgroundAnimatorName);
        jsonObject.put(KEY_FOREGROUND_COLOR,foregroundColor);
        jsonObject.put(KEY_THEME_UI_NAME,themeName);
        jsonObject.put(KEY_THEME_UI_TYPE,themeUIType);
    }

    public void themeUILoad(JSONObject jsonObject) {
        backgroundColor=jsonObject.optInt(KEY_BACKGROUND_COLOR,Color.rgb(0, 0, 0));
        backgroundAnimatorName=jsonObject.optString(KEY_BACKGROUND_ANIMATOR,"");
        foregroundColor=jsonObject.optInt(KEY_FOREGROUND_COLOR,DEFAULT_COLOR);
        themeName=jsonObject.optString(KEY_THEME_UI_NAME,"default");
        themeUIType=jsonObject.optInt(KEY_THEME_UI_TYPE,1);
    }

    protected String getConfigFileName(){
        return Constants.SHARE_PERFERENCE_FILE;
    }

    @Override
    public String toString() {
        return toJsonString();
    }


    public Integer getScreenLockPassword() {
        return screenLockPassword;
    }

    public void setScreenLockPassword(Integer screenLockPassword) {
        this.screenLockPassword = screenLockPassword;
        dirtySave();
    }

    public Integer getHandUpTime() {
        return handUpTime;
    }

    public boolean isHandUpAble() {
        return handUpAble;
    }

    public void setHandUpTime(Integer handUpTime) {
        this.handUpTime = handUpTime;
        dirtySave();
    }

    public void setHandUpAble(boolean handUpAble) {
        this.handUpAble = handUpAble;
        dirtySave();
    }

    public String getHandUpMusicName() {
        return handUpMusicName;
    }

    public void setHandUpMusicName(String handUpMusicName) {
        this.handUpMusicName = handUpMusicName;
        dirtySave();
    }

    public Integer getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Integer backgroundColor) {
        this.backgroundColor = backgroundColor;
        dirtySave();
    }

    public Integer getForegroundColor() {
        return foregroundColor;
    }

    public void setForegroundColor(Integer foregroundColor) {
        this.foregroundColor = foregroundColor;
        dirtySave();
    }

    public Integer getForegroundColor1() {
        return foregroundColor1;
    }

    public void setForegroundColor1(Integer foregroundColor1) {
        this.foregroundColor1 = foregroundColor1;
        dirtySave();
    }

    public Integer getForegroundColor2() {
        return foregroundColor2;
    }

    public void setForegroundColor2(Integer foregroundColor2) {
        this.foregroundColor2 = foregroundColor2;
    }

    public boolean isHourSystem12() {
        return hourSystem12;
    }

    public void setHourSystem12(boolean hourSystem12) {
        this.hourSystem12 = hourSystem12;
        dirtySave();
    }


    public String getHandUpMusic() {
        return handUpMusic;
    }

    public void setHandUpMusic(String handUpMusic) {
        this.handUpMusic = handUpMusic;
        dirtySave();
    }

    public Integer getHandUpDialy() {
        return handUpDialy;
    }

    public void setHandUpDialy(Integer handUpDialy) {
        this.handUpDialy = handUpDialy;
        dirtySave();
    }


    public Boolean isHandUpOverTimeCountingAble() {
        return handUpOverTimeCountingAble;
    }

    public void setHandUpOverTimeCountingAble(Boolean handUpOverTimeCountingAble) {
        this.handUpOverTimeCountingAble = handUpOverTimeCountingAble;
        dirtySave();
    }

    public String getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
        dirtySave();
    }

    public boolean isUseSystemWallpaper() {
        return useSystemWallpaper;
    }

    public void setUseSystemWallpaper(boolean useSystemWallpaper) {
        this.useSystemWallpaper = useSystemWallpaper;
        dirtySave();
    }

    public boolean isBackgroundAnimatorRandomColor() {
        return backgroundAnimatorRandomColor;
    }

    public void setBackgroundAnimatorRandomColor(boolean backgroundAnimatorRandomColor) {
        this.backgroundAnimatorRandomColor = backgroundAnimatorRandomColor;
        dirtySave();
    }

    public int getClockInterfaceType() {
        return clockInterfaceType;
    }

    public void setClockInterfaceType(int clockInterfaceType) {
        this.clockInterfaceType = clockInterfaceType;
        dirtySave();
    }

    public String getThemeName() {
        return themeName;
    }

    public void setThemeName(String themeName) {
        this.themeName = themeName;
        dirtySave();
    }

    public int getThemeUIType() {
        return themeUIType;
    }

    public void setThemeUIType(int themeUIType) {
        this.themeUIType = themeUIType;
        dirtySave();
    }


    public boolean isAutoFullscreen() {
        return autoFullscreen;
    }

    public void setAutoFullscreen(boolean autoFullscreen) {
        this.autoFullscreen = autoFullscreen;
        dirtySave();
    }


    public String getBackgroundAnimatorName() {
        return backgroundAnimatorName;
    }

    public void setBackgroundAnimatorName(String backgroundAnimatorName) {
        this.backgroundAnimatorName = backgroundAnimatorName;
        dirtySave();
    }

    public String getTimeHourAlterTypeName() {
        return timeHourAlterTypeName;
    }

    public void setTimeHourAlterTypeName(String timeHourAlterTypeName) {
        this.timeHourAlterTypeName = timeHourAlterTypeName;
        dirtySave();
    }

    public Integer getTimeHourAlterDialy() {
        return timeHourAlterDialy;
    }

    public void setTimeHourAlterDialy(Integer timeHourAlterDialy) {
        this.timeHourAlterDialy = timeHourAlterDialy;
        dirtySave();
    }

    public String getTimeHourVideoPath() {
        return timeHourVideoPath;
    }

    public void setTimeHourVideoPath(String timeHourVideoPath) {
        this.timeHourVideoPath = timeHourVideoPath;
    }

    //digit

    public Integer[] getTimeColorsArray() {
        return digitPerferenceModel.timeColorsArray;
    }

    public void setTimeColorsArray(Integer[] timeColorsArray) {
        digitPerferenceModel.setTimeColorsArray(timeColorsArray);
        dirtySave();
    }


    public boolean isLinearGradientAble() {
        return digitPerferenceModel.linearGradientAble;
    }

    public void setLinearGradientAble(boolean linearGradientAble) {
        digitPerferenceModel.setLinearGradientAble(linearGradientAble);
        dirtySave();
    }

    public boolean isReflectedAble() {
        return digitPerferenceModel.reflectedAble;
    }

    public void setReflectedAble(boolean reflectedAble) {
        digitPerferenceModel.setReflectedAble(reflectedAble);
        dirtySave();
    }

    public Integer getShadowType() {
        return digitPerferenceModel.shadowType;
    }

    public void setShadowType(Integer shadowType) {
        digitPerferenceModel.setShadowType(shadowType);
        dirtySave();
    }

    public Integer getTimeTextCharAnimatorType() {
        return digitPerferenceModel.timeTextCharAnimatorType;
    }

    public void setTimeTextCharAnimatorType(Integer timeTextCharAnimatorType) {
        digitPerferenceModel.setTimeTextCharAnimatorType(timeTextCharAnimatorType);
        dirtySave();
    }


    public String getFontName() {
        return digitPerferenceModel.fontName;
    }

    public void setFontName(String fontName) {
        digitPerferenceModel.setFontName(fontName);
        dirtySave();
    }

    public Integer getBaseLineX() {
        return digitPerferenceModel.getBaseLineX();
    }

    public void setBaseLineX(Integer baseLineY) {
        digitPerferenceModel.setBaseLineX(baseLineY);
        dirtySave();
    }

    //simulate

    public String getSimulateClockTypeName() {
        return simulatePerferenceModel.simulateClockTypeName;
    }

    public void setSimulateClockTypeName(String simulateClockTypeName) {
        simulatePerferenceModel.setSimulateClockTypeName(simulateClockTypeName);
        dirtySave();
    }

    public String getSimulateClockPointerTypeName() {
        return simulatePerferenceModel.simulateClockPointerTypeName;
    }

    public void setSimulateClockPointerTypeName(String simulateClockPointerTypeName) {
        simulatePerferenceModel.setSimulateClockPointerTypeName(simulateClockPointerTypeName);
        dirtySave();
    }

    public Integer getSimulateClockColorScale() {
        return simulatePerferenceModel.simulateClockColorScale;
    }

    public void setSimulateClockColorScale(Integer simulateClockColorScale) {
        simulatePerferenceModel.setSimulateClockColorScale(simulateClockColorScale);
        dirtySave();
    }

    public Integer getSimulateClockColorScaleParticularly() {
        return simulatePerferenceModel.simulateClockColorScaleParticularly;
    }

    public void setSimulateClockColorScaleParticularly(Integer simulateClockColorScaleParticularly) {
        simulatePerferenceModel.setSimulateClockColorScaleParticularly(simulateClockColorScaleParticularly);
        dirtySave();
    }

    public Integer getSimulateClockColorText() {
        return simulatePerferenceModel.simulateClockColorText;
    }

    public void setSimulateClockColorText(Integer simulateClockColorText) {
        simulatePerferenceModel.setSimulateClockColorText(simulateClockColorText);
        dirtySave();
    }

    public Integer getSimulateClockColorOutLine() {
        return simulatePerferenceModel.simulateClockColorOutLine;
    }

    public void setSimulateClockColorOutLine(Integer simulateClockColorOutLine) {
        simulatePerferenceModel.setSimulateClockColorOutLine(simulateClockColorOutLine);
        dirtySave();
    }

    public Integer getSimulateClockTextShowType() {
        return simulatePerferenceModel.simulateClockTextShowType;
    }

    public void setSimulateClockTextShowType(Integer simulateClockTextShowType) {
        simulatePerferenceModel.setSimulateClockTextShowType(simulateClockTextShowType);
        dirtySave();
    }

    public Integer getSimulateClockColorPointer() {
        return simulatePerferenceModel.simulateClockColorPointer;
    }

    public void setSimulateClockColorPointer(Integer simulateClockColorPointer) {
        simulatePerferenceModel.setSimulateClockColorPointer(simulateClockColorPointer);
        dirtySave();
    }

    public Integer getSimulateClockColorPointerSecond() {
        return simulatePerferenceModel.simulateClockColorPointerSecond;
    }

    public void setSimulateClockColorPointerSecond(Integer simulateClockColorPointerSecond) {
        simulatePerferenceModel.setSimulateClockColorPointerSecond(simulateClockColorPointerSecond);
        dirtySave();
    }

    public String getHandUpTypeName() {
        return handUpTypeName;
    }

    public void setHandUpTypeName(String handUpTypeName) {
        this.handUpTypeName = handUpTypeName;
        dirtySave();
    }

    public DigitPerferenceModel getDigitPerferenceModel() {
        return digitPerferenceModel;
    }

    public void setDigitPerferenceModel(DigitPerferenceModel digitPerferenceModel) {
        this.digitPerferenceModel = digitPerferenceModel;
        dirtySave();
    }

    public SimulatePerferenceModel getSimulatePerferenceModel() {
        return simulatePerferenceModel;
    }

    public void setSimulatePerferenceModel(SimulatePerferenceModel simulatePerferenceModel) {
        this.simulatePerferenceModel = simulatePerferenceModel;
        dirtySave();
    }

    public void setTimeTextSecondSubscript(Boolean secondSubscript) {
        digitPerferenceModel.setTimeTextSecondSubscript(secondSubscript);
        dirtySave();
    }

    public boolean isTimeTextSecondSubscript(){
        return digitPerferenceModel.isTimeTextSecondSubscript();
    }

    public boolean isCharBackgroundBorder() {
        return digitPerferenceModel.isCharBackgroundBorder();
    }

    public void setCharBackgroundBorder(boolean charBackgroundBorder) {
        digitPerferenceModel.setCharBackgroundBorder(charBackgroundBorder);
        dirtySave();
    }

    public Integer getCharBackgroundBorderColor() {
        return digitPerferenceModel.getCharBackgroundBorderColor();
    }

    public void setCharBackgroundBorderColor(Integer charBackgroundBorderColor) {
        digitPerferenceModel.setCharBackgroundBorderColor(charBackgroundBorderColor);
        dirtySave();
    }

    public Integer getBaseLineDown() {
        return digitPerferenceModel.getBaseLineDown();
    }

    public void setBaseLineDown(Integer baseLineDown) {
        digitPerferenceModel.setBaseLineDown(baseLineDown);
        dirtySave();
    }

    public Integer getCharBackgroundBorderDividerColor() {
        return digitPerferenceModel.getCharBackgroundBorderDividerColor();
    }

    public void setCharBackgroundBorderDividerColor(Integer charBackgroundBorderDividerColor) {
        digitPerferenceModel.setCharBackgroundBorderDividerColor(charBackgroundBorderDividerColor);
        dirtySave();
    }

    public Integer getCharBackgroundBorderDividerStrokeWidth() {
       return digitPerferenceModel.getCharBackgroundBorderDividerStrokeWidth();
    }

    public void setCharBackgroundBorderDividerStrokeWidth(Integer charBackgroundBorderDividerStrokeWidth) {
        digitPerferenceModel.setCharBackgroundBorderDividerStrokeWidth(charBackgroundBorderDividerStrokeWidth);
        dirtySave();
    }

    public boolean isBootStart() {
        return bootStart;
    }

    public void setBootStart(boolean bootStart) {
        this.bootStart = bootStart;
        dirtySave();
    }

    public Integer getSubscriptFontScale() {
        return digitPerferenceModel.getSubscriptFontScale();
    }

    public void setSubscriptFontScale(Integer subscriptFontScale) {
        digitPerferenceModel.setSubscriptFontScale(subscriptFontScale);
        dirtySave();
    }

    public Integer getTimeTextPadding() {
        return digitPerferenceModel.getTimeTextPadding();
    }

    public void setTimeTextPadding(Integer timeTextPadding) {
        digitPerferenceModel.setTimeTextPadding(timeTextPadding);
        dirtySave();
    }

    public boolean isCharBackgroundBorderWithDoubble() {
        return digitPerferenceModel.isCharBackgroundBorderWithDoubble();
    }

    public void setCharBackgroundBorderWithDoubble(boolean charBackgroundBorderWithDoubble) {
        digitPerferenceModel.setCharBackgroundBorderWithDoubble(charBackgroundBorderWithDoubble);
        dirtySave();
    }

    public Integer getFontStyleSize() {
        return digitPerferenceModel.getFontStyleSize();
    }

    public void setFontStyleSize(Integer fontStyleSize) {
        digitPerferenceModel.setFontStyleSize(fontStyleSize);
        dirtySave();
    }

    public boolean isFirstStart() {
        return firstStart;
    }

    public void setFirstStart(boolean firstStart) {
        this.firstStart = firstStart;
        dirtySave();
    }

    public boolean isFullscreenSpiritAble() {
        return fullscreenSpiritAble;
    }

    public void setFullscreenSpiritAble(boolean fullscreenSpiritAble) {
        this.fullscreenSpiritAble = fullscreenSpiritAble;
        dirtySave();
    }

    public boolean isTodoSyncAble() {
        return todoSyncAble;
    }

    public void setTodoSyncAble(boolean todoSyncAble) {
        this.todoSyncAble = todoSyncAble;
        dirtySave();
    }

    public boolean isBackgroundImageVisable() {
        return backgroundImageVisable;
    }

    public void setBackgroundImageVisable(boolean backgroundImageVisable) {
        this.backgroundImageVisable = backgroundImageVisable;
        dirtySave();
    }
}
