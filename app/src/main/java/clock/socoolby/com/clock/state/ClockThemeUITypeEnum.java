package clock.socoolby.com.clock.state;

public enum ClockThemeUITypeEnum {
    NORMAL(0),SETTING(1),FULLSCREEN(2);

    public  int code;

    ClockThemeUITypeEnum(int code) {
        this.code = code;
    }

    public static ClockThemeUITypeEnum valueOf(int code){
        switch (code){
            case 1:
                return SETTING;
            case 2:
                return FULLSCREEN;
        }
        return NORMAL;
    }
}
