package clock.socoolby.com.clock.widget.animatorview.animator;


//引用自:https://github.com/YuToo/FluorescenceView/

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader;

import java.util.LinkedList;
import java.util.List;

import clock.socoolby.com.clock.widget.animatorview.AbstractCacheAbleAnimator;
import clock.socoolby.com.clock.widget.animatorview.I_AnimatorEntry;

public class FluorescenceAnimator extends AbstractCacheAbleAnimator<FluorescenceAnimator.Particle> {

    public static final String NAME="Fluorescence";

    private int     mParticleRadius = 15;          // 粒子大小基数
    private int     mParticleRandomRadius = 50;    // 随机范围(基数上范围)
    private int     mParticleLife = 3000;          //生命基数（毫秒）
    private int     mParticleRandomLife = 8000;        //随机范围（基数上范围）
    private int     mParticleNum = 20;           //粒子数量
    //private int[]   mParticleColors = {0xFF0d4289, 0xff034aa1,0x887b0808, 0xff176bd1, 0xff1f39ff,0x33d4ed00, 0x66ffffff, 0xff777800, 0xff0e2569};//粒子颜色集合

    List<Particle> cache = new LinkedList<>();

    public FluorescenceAnimator() {
        super(DYNAMIC_QUANTITY);
    }

    public FluorescenceAnimator(int mParticleNum) {
        super(DYNAMIC_QUANTITY);
        this.mParticleNum=mParticleNum;
    }

    @Override
    public boolean run() {
        for(Particle particle : list){
            particle.move(width,height);
            if(particle.getLife()<=0)
                cache.add(particle);
        }
        moveToTrashCache(cache);
        list.removeAll(cache);
        for(int i = 0 ; i < mParticleNum - list.size() ; i ++){
            list.add(createNewEntry());
        }
        cache.clear();
        return true;
    }

    Particle newParticle;
    @Override
    public Particle createNewEntry() {
        newParticle =revectForTrashCache();
        if(newParticle ==null)
            newParticle =new Particle();
        newParticle.setStartTime();
        newParticle.setStartPointF(rand.nextInt(width), rand.nextInt(height));
        newParticle.setEndPointF(rand.nextInt(width), rand.nextInt(height));
        // 随机生命
        newParticle.setLife(mParticleLife + rand.nextInt(mParticleRandomLife));
        // 随机大小
        newParticle.setRadius(mParticleRadius + rand.nextInt(mParticleRandomRadius));
        // 随机颜色
        randomColorIfAble();
        newParticle.setParticleColor(color);
        return newParticle;
    }

    /**
     * Created by YuToo on 2017/2/28.
     * 荧光对象
     */
    public static class Particle implements I_AnimatorEntry {

        private float startPointX,startPointY;//荧光开始坐标
        private float endPointX,endPointY;//荧光结束点坐标
        private float pointX,pointY;//当前坐标
        private float radius;// 荧光半径
        private long startTime;//开始时间
        private int life;   //生命
        private int particleColor;//颜色

        private float translate;
        private Shader shader;
        private Paint particlePaint=new Paint();

        public Particle(){
        }

        public void setStartPointF(float startPointX,float startPointY) {
            this.startPointX=startPointX;
            this.startPointY=startPointY;
        }

        public void setEndPointF(float endPointX,float endPointY) {
           this.endPointX=endPointX;
           this.endPointY=endPointY;
        }

        public float getRadius() {
            return radius;
        }

        public void setRadius(float radius) {
            this.radius = radius;
        }

        public void setLife(int life) {
            this.life = life;
        }

        public int getParticleColor() {
            return particleColor;
        }

        public void setParticleColor(int particleColor) {
            this.particleColor = particleColor;
            shader=null;
        }


        //获取剩余生命
        public int getLife(){
            return (int)(startTime + life - System.currentTimeMillis());
        }

        @Override
        public void move(int maxWidth, int maxHight) {
            int life = getLife();
            if(life <= 0)
                 return;
            float x = (life * 1.0f / this.life);
            pointX = endPointX + (endPointX - startPointX) * x;
            pointY = endPointY + (endPointY - startPointY) * x;
            //获取粒子透明度:先透明再实体再透明（二次函数）
            translate=4 * x *(1 - x);
        }


        @Override
        public void onDraw(Canvas canvas,Paint mPaint) {
            if(shader==null) {
                shader = new RadialGradient(0, 0, radius, particleColor, 0x00000000, Shader.TileMode.CLAMP);
                particlePaint.setShader(shader);
            }
            particlePaint.setAlpha((int)(translate * 255));
            canvas.save();
            canvas.translate(pointX, pointY);
            canvas.drawCircle(0, 0, radius, particlePaint);
            canvas.restore();
        }

        @Override
        public void setAnimatorEntryColor(int color) {
            this.particleColor =color;
            shader=null;
        }

        public void setStartTime() {
            startTime = System.currentTimeMillis();
        }
    }
}
