package clock.socoolby.com.clock.fragment.simulate;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import clock.socoolby.com.clock.ClockApplication;
import clock.socoolby.com.clock.R;
import clock.socoolby.com.clock.state.ClockStateMachine;
import clock.socoolby.com.clock.viewmodel.GlobalViewModel;
import clock.socoolby.com.clock.viewmodel.SimulateViewModel;
import clock.socoolby.com.clock.viewmodel.ViewModelFactory;
import clock.socoolby.com.clock.widget.animatorview.AnimatorView;
import clock.socoolby.com.clock.widget.animatorview.animator.ClockAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.AbstractClock;
import clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.ClockFactory;
import clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.pointer.PointerFactory;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class SimulateClockFragment extends Fragment {
    public static final String TAG=SimulateClockFragment.class.getSimpleName();

    SimulateViewModel simulateViewModel;
    GlobalViewModel globalViewModel;

    AnimatorView clockView;

    ClockAnimator clockAnimator=null;

    ClockStateMachine clockStateMachine;

    public SimulateClockFragment(){
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        clockAnimator = new ClockAnimator();
        simulateViewModel= ViewModelProviders.of(getActivity(), new ViewModelFactory(ClockApplication.getInstance().getModel())).get(SimulateViewModel.class);
        globalViewModel=ViewModelProviders.of(getActivity(), new ViewModelFactory(ClockApplication.getInstance().getModel())).get(GlobalViewModel.class);
        clockStateMachine=globalViewModel.getClockStateMachine();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_simulate_clock, container, false);
        clockView=view.findViewById(R.id.tv_foreground_animatorview);
        clockAnimator.init(clockView.getContext(),clockView);
        clockView.setAnimator(clockAnimator);
        bindViewModel();
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        clockAnimator.start();
    }

    @Override
    public void onPause() {
        super.onPause();
        clockAnimator.pause();
    }

    private void bindViewModel(){

        simulateViewModel.getClockTypeName().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                setClockStyle(s);
            }
        });

        globalViewModel.getForegroundColor().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                clockAnimator.setColor(integer);
            }
        });

        simulateViewModel.getSimulateClockColorOutLine().observe(this,color->clockAnimator.setmClockColor(color));

        simulateViewModel.getSimulateClockColorPointer().observe(this,color->clockAnimator.setmClockColorPointer(color));

        simulateViewModel.getSimulateClockColorPointerSecond().observe(this,color->clockAnimator.setPointerSecondColor(color));

        simulateViewModel.getSimulateClockColorScale().observe(this,color->clockAnimator.setmColorDefaultScale(color));

        simulateViewModel.getSimulateClockColorScaleParticularly().observe(this,color->clockAnimator.setmColorParticularyScale(color));

        simulateViewModel.getPointerTypeName().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                setClockPointer(s);
            }
        });
    }


    AbstractClock clock;

    private void setClockStyle(String clockStyle){
        //clockAnimator.setParticleColor(globalViewModel.getForegroundColor().getValue());
        clock= ClockFactory.build(clockStyle);
        clockAnimator.setClock(clock);
        clockAnimator.setClockPointer(PointerFactory.build(simulateViewModel.getPointerTypeName().getValue()));
        if(!clockAnimator.isRunning())
            clockAnimator.start();
    }

    private void setClockPointer(String clockPointerStyle){
        clockAnimator.setClockPointer(PointerFactory.build(clockPointerStyle));
    }
}
