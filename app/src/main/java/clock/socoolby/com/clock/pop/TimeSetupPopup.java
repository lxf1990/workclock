package clock.socoolby.com.clock.pop;

import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import clock.socoolby.com.clock.R;
import clock.socoolby.com.clock.fragment.handup.HandUpDefaultFragment;
import clock.socoolby.com.clock.fragment.handup.HandUpSelect123Fragment;
import io.feeeei.circleseekbar.CircleSeekBar;
import xyz.aprildown.ultimatemusicpicker.MusicPickerListener;
import xyz.aprildown.ultimatemusicpicker.UltimateMusicPicker;

public class TimeSetupPopup extends FullScreenDialogFragment implements MusicPickerListener {

    public static final String TAG=TimeSetupPopup.class.getName();

    private CircleSeekBar mHourSeekbar;

    private CircleSeekBar mMinuteSeekbar;

    private TextView mTextView;

    private int hour=0,minute=0;

    OnTimeChangeListener changeListener;

    Uri uri;

    String s;

    Button musicSelect;

    ImageView back;

    RadioGroup handUpAlterTypeGroup;

    RadioButton handUpAlterTypeDefault;

    RadioButton handUpAlterTypeSelect;

    String handUpAlterType;

    ImageView ok;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View content=inflater.inflate(R.layout.pop_union_time,container);
        mHourSeekbar = (CircleSeekBar) content.findViewById(R.id.seek_hour);
        mMinuteSeekbar = (CircleSeekBar) content.findViewById(R.id.seek_minute);
        mTextView = (TextView)content.findViewById(R.id.seek_time);

        mTextView.setTextColor(Color.YELLOW);

        musicSelect=content.findViewById(R.id.tv_handup_time_select_Music);

        back=content.findViewById(R.id.tv_handup_time_back);

        back.setOnClickListener(view-> dismiss());

        ok=content.findViewById(R.id.tv_handup_time_ok);

        ok.setOnClickListener(view->{
            if(changeListener!=null)
                changeListener.onChanged(hour,minute,uri,s,handUpAlterType);
            dismiss();
        });


        musicSelect.setOnClickListener(v -> {
            UltimateMusicPicker picker=new UltimateMusicPicker();
            picker.removeSilent().ringtone().alarm().music().notification().goWithDialog(getChildFragmentManager());
        });

        mTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(changeListener!=null)
                    changeListener.onChanged(hour,minute,uri,s,handUpAlterType);
                dismiss();
            }
        });

        mHourSeekbar.setOnSeekBarChangeListener(new CircleSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onChanged(CircleSeekBar seekbar, int curValue) {
                hour=curValue;
                changeText(hour,minute);
            }
        });

        mMinuteSeekbar.setOnSeekBarChangeListener(new CircleSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onChanged(CircleSeekBar seekbar, int curValue) {
                minute=curValue;
                changeText(hour,minute);
            }
        });

        handUpAlterTypeGroup=content.findViewById(R.id.tv_huad_up_style_group);

        handUpAlterTypeDefault=content.findViewById(R.id.tv_huad_up_style_default);

        handUpAlterTypeSelect=content.findViewById(R.id.tv_huad_up_style_select);

        handUpAlterTypeGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                handUpAlterType = HandUpDefaultFragment.NAME;
                switch (checkedId){
                    case R.id.tv_huad_up_style_select:
                        handUpAlterType = HandUpSelect123Fragment.NAME;
                        break;
                }

            }
        });

        return content;
    }

    @Override
    public void onResume() {
        super.onResume();
        timber.log.Timber.d("onResume hour:"+hour+"\tminute:"+minute);
        mHourSeekbar.fillInside();
        mHourSeekbar.setCurProcess(hour);
        mMinuteSeekbar.setCurProcess(minute);
        //changeText(hour,minute);
        musicSelect.setText(s);
        switch (handUpAlterType){
            case HandUpSelect123Fragment.NAME:
                handUpAlterTypeSelect.setChecked(true);
                break;
            case HandUpDefaultFragment.NAME:
                handUpAlterTypeDefault.setChecked(true);
        }
    }



    public void init(int hour, int minute,String handUpAlterType,Uri musicUri,String musicName) {
        this.hour=hour;
        this.minute=minute;
        this.s=musicName;
        this.uri=musicUri;
        this.handUpAlterType=handUpAlterType;
    }

    private void changeText(int hour, int minute) {
        int day=hour/60;
        int inHour=hour;
        String hourStr="";
        if(hour>=60) {
            hourStr = day > 9 ? day + "" : "0" + day + ":";
            inHour=hour-day*60;
        }
         hourStr += inHour > 9 ? inHour + "" : "0" + inHour;
        String minuteStr = minute > 9 ? minute + "" : "0" + minute;
        mTextView.setText(hourStr + ":" + minuteStr);
    }


    public void setOnSeekBarChangeListener(OnTimeChangeListener l){
        this.changeListener=l;
    }


    @Override
    public void onMusicPick(Uri uri, String s) {
          Log.d("time Setup","on music pick:"+uri.toString());
          this.uri=uri;
          this.s=s;
          musicSelect.setText(s);
    }

    @Override
    public void onPickCanceled() {
        this.uri=null;
        this.s="default";
        musicSelect.setText(s);
    }

    public interface OnTimeChangeListener {
        void onChanged(int hour, int minute,Uri uri, String s,String hundUpStyle);
    }

}