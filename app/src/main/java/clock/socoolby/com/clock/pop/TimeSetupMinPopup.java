package clock.socoolby.com.clock.pop;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.widget.TextView;
import clock.socoolby.com.clock.R;
import io.feeeei.circleseekbar.CircleSeekBar;
import razerdp.basepopup.BasePopupWindow;

public class TimeSetupMinPopup extends BasePopupWindow {
    private CircleSeekBar mHourSeekbar;

    private CircleSeekBar mMinuteSeekbar;

    private TextView mTextView;

    private int hour=0,minute=0;

    OnTimeChangeListener changeListener;

    public TimeSetupMinPopup(Context context) {
        super(context);
    }


    // 必须实现，这里返回您的contentView
    // 为了让库更加准确的做出适配，强烈建议使用createPopupById()进行inflate
    @Override
    public View onCreateContentView() {
        View content=createPopupById(R.layout.pop_union_time_min);
        mHourSeekbar = (CircleSeekBar) content.findViewById(R.id.seek_hour);
        mMinuteSeekbar = (CircleSeekBar) content.findViewById(R.id.seek_minute);
        mTextView = (TextView)content.findViewById(R.id.seek_time);

        mTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(changeListener!=null)
                    changeListener.onChanged(hour,minute);
                TimeSetupMinPopup.this.dismiss();
            }
        });

        mHourSeekbar.setOnSeekBarChangeListener(new CircleSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onChanged(CircleSeekBar seekbar, int curValue) {
                changeText(curValue, mMinuteSeekbar.getCurProcess());
            }
        });
        mHourSeekbar.fillInside();

        mMinuteSeekbar.setOnSeekBarChangeListener(new CircleSeekBar.OnSeekBarChangeListener() {
            @Override
            public void onChanged(CircleSeekBar seekbar, int curValue) {
                changeText(mHourSeekbar.getCurProcess(), curValue);
            }
        });

        mHourSeekbar.setCurProcess(hour);
        mMinuteSeekbar.setCurProcess(minute);

        return content;
    }

    public void init(int hour, int minute) {
        this.hour=hour;
        this.minute=minute;
        if(mHourSeekbar!=null) {
            mHourSeekbar.setCurProcess(hour);
            mMinuteSeekbar.setCurProcess(minute);
        }
    }

    private void changeText(int hour, int minute) {
        this.hour=hour;
        this.minute=minute;
        int day=hour/60;
        int inHour=hour;
        String hourStr="";
        if(hour>=60) {
            hourStr = day > 9 ? day + "" : "0" + day + ":";
            inHour=hour-day*60;
        }
         hourStr += inHour > 9 ? inHour + "" : "0" + inHour;
        String minuteStr = minute > 9 ? minute + "" : "0" + minute;
        mTextView.setText(hourStr + ":" + minuteStr);
    }


    // 以下为可选代码（非必须实现）
    // 返回作用于PopupWindow的show和dismiss动画，本库提供了默认的几款动画，这里可以自由实现
    @Override
    protected Animation onCreateShowAnimation() {
        return getDefaultScaleAnimation(true);
    }

    @Override
    protected Animation onCreateDismissAnimation() {
        return getDefaultScaleAnimation(false);
    }

    public void setOnSeekBarChangeListener(OnTimeChangeListener l){
        this.changeListener=l;
    }

    public interface OnTimeChangeListener {
        void onChanged(int hour, int minute);
    }

}