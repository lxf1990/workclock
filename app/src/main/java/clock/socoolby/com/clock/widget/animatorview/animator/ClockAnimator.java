package clock.socoolby.com.clock.widget.animatorview.animator;


import android.graphics.Color;
import android.util.Log;

import clock.socoolby.com.clock.widget.animatorview.AbstractAnimator;
import clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.AbstractClock;
import clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.I_Pointer;
import clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.pointer.DefaultPointer;
import clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.pointer.LeafPointer;
import clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.pointer.LeafTwoPointer;
import clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.pointer.SecondTailPointer;
import clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.pointer.SwordPointer;
import clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.pointer.TrianglePointer;
import clock.socoolby.com.clock.widget.animatorview.animator.clockanimator.pointer.TwoStepPointer;

// 参考自:https://github.com/ifadai/ClockDemo
public class ClockAnimator extends AbstractAnimator<AbstractClock> {
    private final String TAG = getClass().getSimpleName();

    AbstractClock clock;

    // 时钟颜色、默认刻度颜色、时刻度颜色
    protected int mClockColor=Color.BLACK, mColorDefaultScale=Color.BLACK, mColorParticularyScale=Color.BLACK,textColor=Color.BLACK,mClockColorPointer=Color.BLACK,pointerSecondColor=Color.BLACK;

    public ClockAnimator() {
        super(DYNAMIC_QUANTITY);
    }

    @Override
    public AbstractClock createNewEntry() {
        return null;
    }

    @Override
    public boolean run() {
        //Log.d("clock","animator is running: "+(clock!=null));
        if(clock==null)
            return false;
        return super.run();
    }

    public void setClock(AbstractClock clock1){
        list.clear();
        this.clock=clock1;
        int minHigh=Math.min(width,height);
        clock.init(width,height,width/2,height/2,minHigh/2,color,textColor);

        clock.setPointerSecondColor(pointerSecondColor);
        clock.setmColorDefaultScale(mColorDefaultScale);
        clock.setmColorParticularyScale(mColorParticularyScale);
        clock.setTextColor(textColor);
        clock.setmClockColorPointer(mClockColorPointer);
        clock.setPointerSecondColor(pointerSecondColor);
        clock.setmClockColor(mClockColor);

        list.add(clock);
        Log.d("clock","set clock type: "+clock1.typeName()+"\t color:"+ color);
    }

    public void setClockPointer(I_Pointer pointer){
        clock.setPointer(pointer);
        clock.setmClockColorPointer(mClockColorPointer);
        clock.setPointerSecondColor(pointerSecondColor);
    }

    @Override
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if(clock!=null) {
            int minHigh=Math.min(width,height);
            clock.init(width, height, width / 2, height / 2, minHigh / 2, color, textColor);
        }
    }

    public void setmClockColor(int mClockColor) {
        this.mClockColor = mClockColor;
        clock.setmClockColor(mClockColor);
    }

    public void setmColorDefaultScale(int mColorDefaultScale) {
        this.mColorDefaultScale = mColorDefaultScale;
        clock.setmColorDefaultScale(mColorDefaultScale);
    }

    public void setmColorParticularyScale(int mColorParticularyScale) {
        this.mColorParticularyScale = mColorParticularyScale;
        clock.setmColorParticularyScale(mColorParticularyScale);
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
        clock.setTextColor(textColor);
    }

    public void setmClockColorPointer(int mClockColorPointer) {
        this.mClockColorPointer = mClockColorPointer;
        clock.setmClockColorPointer(mClockColorPointer);
    }

    public void setPointerSecondColor(int pointerSecondColor) {
        this.pointerSecondColor = pointerSecondColor;
        clock.setPointerSecondColor(pointerSecondColor);
    }
}
