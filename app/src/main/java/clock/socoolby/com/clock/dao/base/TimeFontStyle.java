package clock.socoolby.com.clock.dao.base;

import e.odbo.data.bean.BaseBean;

public class TimeFontStyle  extends BaseBean<String> {
    //private static final long serialVersionUID =-2865594038566020367L;

    public final static String NAME="NAME";//字体名称
    public final static String DISPLAY_SECOND="DISPLAY_SECOND";//有秒时的大小
    public final static String NO_DISPLAY_SECOND="NO_DISPLAY_SECOND";//无秒时的大小
    public final static String DISPLAY_SECOND_ON_FULL="DISPLAY_SECOND_ON_FULL";//全屏有秒时的大小
    public final static String NO_DISPLAY_SECOND_ON_FULL="NO_DISPLAY_SECOND_ON_FULL";//全屏无秒时的大小

    public String name;
    public int displaySecond;
    public int noDisplaySecond;
    public int displaySecondOnFull;
    public int noDisplaySecondOnFull;

    public TimeFontStyle() {
        super();
    }

    public TimeFontStyle(String name, int displaySecond, int noDisplaySecond) {
        this(name,displaySecond,noDisplaySecond,displaySecond,noDisplaySecond);
    }

    public TimeFontStyle(String name, int displaySecond, int noDisplaySecond, int displaySecondOnFull, int noDisplaySecondOnFull) {
        this.name = name;
        this.displaySecond = displaySecond;
        this.noDisplaySecond = noDisplaySecond;
        this.displaySecondOnFull = displaySecondOnFull;
        this.noDisplaySecondOnFull = noDisplaySecondOnFull;
    }

    @Override
    public String getKey() {
        return name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDisplaySecond() {
        return displaySecond;
    }

    public void setDisplaySecond(Integer displaySecond) {
        this.displaySecond = displaySecond;
    }

    public Integer getNoDisplaySecond() {
        return noDisplaySecond;
    }

    public void setNoDisplaySecond(Integer noDisplaySecond) {
        this.noDisplaySecond = noDisplaySecond;
    }

    public Integer getDisplaySecondOnFull() {
        return displaySecondOnFull;
    }

    public void setDisplaySecondOnFull(Integer displaySecondOnFull) {
        this.displaySecondOnFull = displaySecondOnFull;
    }

    public Integer getNoDisplaySecondOnFull() {
        return noDisplaySecondOnFull;
    }

    public void setNoDisplaySecondOnFull(Integer noDisplaySecondOnFull) {
        this.noDisplaySecondOnFull = noDisplaySecondOnFull;
    }

    public String toString() {
        return "name="+name
                +",displaySecond="+displaySecond
                +",noDisplaySecond="+noDisplaySecond
                +",displaySecondOnFull="+displaySecondOnFull
                +",noDisplaySecondOnFull="+noDisplaySecondOnFull;
    }
}
