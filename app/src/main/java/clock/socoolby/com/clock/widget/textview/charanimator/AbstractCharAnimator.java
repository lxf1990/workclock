package clock.socoolby.com.clock.widget.textview.charanimator;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.animation.Interpolator;

public abstract class AbstractCharAnimator {
    protected boolean charAnimatorRuning;
    protected float charAnimatorPercent,realCharAnimatorPercent;

    protected String preString,currentString;

    protected float runSpeed;

    protected Interpolator interpolator;

    public AbstractCharAnimator(String preString, String currentString) {
        this(preString,currentString,0.05f);
    }

    public AbstractCharAnimator(String preString, String currentString,float runSpeed) {
        this.preString = preString;
        this.currentString = currentString;
        charAnimatorRuning=true;
        realCharAnimatorPercent=charAnimatorPercent=0f;
        this.runSpeed=runSpeed;
    }

    public void drawCharAnimator(Canvas canvas, float startX, float startY, Paint mTextPaint){
        drawCharPre(canvas, preString, startX, startY, mTextPaint,charAnimatorPercent);
        drawCharCurrent(canvas, currentString, startX, startY, mTextPaint,realCharAnimatorPercent);
        move();
    }

    public abstract void drawCharPre(Canvas canvas,String strToDraw, float startX, float startY, Paint mTextPaint,final float percent);

    public abstract void drawCharCurrent(Canvas canvas,String strToDraw, float startX, float startY, Paint mTextPaint,final float percent);

    public  void move(){
        charAnimatorPercent+=runSpeed;
        if(null!=interpolator)
           realCharAnimatorPercent=interpolator.getInterpolation(charAnimatorPercent);
        else
           realCharAnimatorPercent=charAnimatorPercent;

        if(charAnimatorPercent>1)
            charAnimatorRuning=false;
    }

    public boolean isCharAnimatorRuning() {
        return charAnimatorRuning;
    }

    public void setInterpolator(Interpolator interpolator) {
        this.interpolator = interpolator;
    }
}
