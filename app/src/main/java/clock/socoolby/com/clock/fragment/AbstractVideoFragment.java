package clock.socoolby.com.clock.fragment;

import android.content.res.AssetFileDescriptor;
import android.graphics.SurfaceTexture;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import java.io.IOException;

import clock.socoolby.com.clock.ClockApplication;
import clock.socoolby.com.clock.utils.Player;
import clock.socoolby.com.clock.viewmodel.GlobalViewModel;
import clock.socoolby.com.clock.viewmodel.ViewModelFactory;

public abstract class AbstractVideoFragment extends Fragment {
    protected String videoFileName;

    protected MediaPlayer mediaPlayer;

    protected int fileLocal=0;//0-》自定，1-》assets

    public AbstractVideoFragment(String videoFileName) {
        this.videoFileName = videoFileName;
    }

    public AbstractVideoFragment() {
    }


    @Override
    public void onResume() {
        super.onResume();
        Player.getInstance().setPlayAble(false);
        try {
            if(fileLocal==1) {
                AssetFileDescriptor fd = getActivity().getAssets().openFd(videoFileName);
                mediaPlayer.setDataSource(fd.getFileDescriptor(), fd.getStartOffset(), fd.getLength());
            }else
               mediaPlayer.setDataSource(videoFileName);
            mediaPlayer.prepare();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.seekTo(0);
                    mp.stop();
                    onPlayCompletion();
                }
            });
            mediaPlayer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void initMediaPlay(TextureView textureView){
        mediaPlayer=new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        textureView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                // SurfaceTexture准备就绪
                mediaPlayer.setSurface(new Surface(surface));
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
                // SurfaceTexture缓冲大小变化
            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                // SurfaceTexture即将被销毁
                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surface) {
                // SurfaceTexture通过updateImage更新
            }
        });
        textureView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textureViewClick();
            }
        });
    }

    protected abstract void onPlayCompletion();

    protected void textureViewClick(){

    }

    @Override
    public void onPause() {
        super.onPause();
        mediaPlayer.pause();

    }

    @Override
    public void onStop() {
        super.onStop();
        if(mediaPlayer.isPlaying())
            mediaPlayer.stop();
        mediaPlayer.release();
        Player.getInstance().setPlayAble(true);
    }
}
