package clock.socoolby.com.clock.todo.microsoft.utils;

import android.annotation.SuppressLint;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class TypeUtils {
    public static final String TAG=TypeUtils.class.getCanonicalName();

    public static final SimpleDateFormat DATE_UTC_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.'SSS'Z'");

    public static final SimpleDateFormat DAY_FORMAT =new SimpleDateFormat("yyyy-MM-dd");

    public static final SimpleDateFormat DAY_TIME_FORMAT =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static final SimpleDateFormat TIME_FORMAT =new SimpleDateFormat("HH:mm:ss");

    public static String dateUtcFormat(Date date){
       return DATE_UTC_FORMAT.format(date);
    }

    public static String dateFormat(Date date){
        return DAY_TIME_FORMAT.format(date);
    }

    public static String dayFormat(Date date){
        return DAY_FORMAT.format(date);
    }

    public static String dateUtcFormat(Calendar date){
        return DATE_UTC_FORMAT.format(date.getTime());
    }
    public static String timeFormat(Date date){
        return TIME_FORMAT.format(date);
    }

    /**
     * Deserializes an ISO-8601 formatted date.
     * @param strVal The string val.
     * @return The calendar.
     * @throws java.text.ParseException The parse exception.
     */
    public static Calendar deserializeToCalendar(final String strVal) throws ParseException {
        final Calendar calendar = java.util.Calendar.getInstance();
        calendar.setTime(deserialize(strVal));
        return calendar;
    }

    public static Date deserialize(final String strVal) throws ParseException {
        return deserialize(strVal,null);
    }

    public static Date deserialize(final String strVal,String timeZone) throws ParseException {

        if(null==strVal||strVal.isEmpty())
            return null;

        // Change Z to +0000 to adapt the string to a format
        // that can be parsed in Java
        final boolean hasZ = strVal.indexOf('Z') != -1;
        String modifiedStrVal;
        final String zSuffix;
        if (hasZ) {
            zSuffix = "Z";
            modifiedStrVal = strVal.replace("Z", "+0000");
        } else {
            zSuffix = "";
            modifiedStrVal = strVal;
        }

        //timber.log.Timber.d("string to deserialize date:"+modifiedStrVal);

        // Parse the well-formatted date string.
        final String datePattern;
        if (modifiedStrVal.contains(".")&&modifiedStrVal.contains("+")) {
            //SimpleDateFormat only supports 3 milliseconds
            String milliseconds = modifiedStrVal.substring(modifiedStrVal.indexOf(".") + 1,
                    modifiedStrVal.indexOf("+"));
            final int millisSegmentLength = 3;
            if (milliseconds.length() > millisSegmentLength) {
                milliseconds = milliseconds.substring(0, millisSegmentLength);
                modifiedStrVal = modifiedStrVal.substring(0,
                        modifiedStrVal.indexOf(".") + 1)
                        + milliseconds
                        + modifiedStrVal.substring(modifiedStrVal.indexOf("+"));
            }
            datePattern = "yyyy-MM-dd'T'HH:mm:ss.SSS" + zSuffix;
        } else {
            datePattern = "yyyy-MM-dd'T'HH:mm:ss" + zSuffix;
        }

        @SuppressLint("SimpleDateFormat")
        final SimpleDateFormat dateFormat = new SimpleDateFormat(datePattern);
        if(timeZone==null)
           dateFormat.setTimeZone(TimeZone.getDefault());
        else
           dateFormat.setTimeZone(TimeZone.getTimeZone(timeZone));

        return dateFormat.parse(modifiedStrVal);
    }

    public static String dealDateFormat(String oldDateStr) throws ParseException{
        //此格式只有  jdk 1.7才支持  yyyy-MM-dd'T'HH:mm:ss.SSSXXX
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");  //yyyy-MM-dd'T'HH:mm:ss.SSSZ
        Date  date = df.parse(oldDateStr);
        SimpleDateFormat df1 = new SimpleDateFormat ("EEE MMM dd HH:mm:ss Z yyyy", Locale.UK);
        Date date1 =  df1.parse(date.toString());
        return DAY_TIME_FORMAT.format(date1);
    }

    public static Date deserializeNotTime(final String strVal) throws ParseException {
        if(null==strVal||strVal.isEmpty())
            return null;
        return DAY_FORMAT.parse(strVal);
    }
}
