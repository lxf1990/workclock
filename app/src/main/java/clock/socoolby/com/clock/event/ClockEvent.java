package clock.socoolby.com.clock.event;

import androidx.annotation.NonNull;

import clock.socoolby.com.clock.model.DateModel;
import clock.socoolby.com.clock.todo.microsoft.bean.todo.TodoEntity;

public class ClockEvent<T> {
    public final static String TALKING="clock_talking";

    public final static String HANDUP="clock_handup";

    public final static String HOUR_ANIMATOR="clock_hour_animator";

    public final static String LONG_SLEEP_WAKE_UP="clock_long_sleep_wake_up";

    public final static String HEARTBEAT="heartbeat";

    public final static String COUNTING_STATE_CHANGED="clock_counting_state_changed";

    public final static String NEW_DAY="clock_new_day";

    public final static String TODO_SYNC_SUCCESS="clock_todo_sync_success";

    public final static String TODO_ALTER="clock_todo_alter";


    private String eventType;
    private  T value;

    public ClockEvent(@NonNull String eventType, @NonNull T value) {
        this.eventType = eventType;
        this.value = value;
    }


    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public  ClockEvent<T> value(T value){
        this.value = value;
        return this;
    }

    public static <T> ClockEvent<T> build(String name ,T value){
        return new ClockEvent<>(name,value);
    }

    public static  ClockEvent<Integer> buildTalking(int typeValue){
        return new ClockEvent<>(TALKING,typeValue);
    }

    public static  ClockEvent<Boolean> buildHandup(boolean value){
        return new ClockEvent<>(HANDUP,value);
    }

    public static  ClockEvent<Boolean> buildHeartbeat(boolean value){
        return new ClockEvent<>(HEARTBEAT,value);
    }

    public static  ClockEvent<Boolean> buildHourAnimator(boolean value){
        return new ClockEvent<>(HOUR_ANIMATOR,value);
    }

    public static  ClockEvent<Integer> buildLongSleepWakeUp(int value){
        return new ClockEvent<>(LONG_SLEEP_WAKE_UP,value);
    }

    public static  ClockEvent<Boolean> buildCountingStateChanged(boolean value){
        return new ClockEvent<>(COUNTING_STATE_CHANGED,value);
    }

    public static  ClockEvent<DateModel> buildNewDay(DateModel value){
        return new ClockEvent<>(NEW_DAY,value);
    }

    public static  ClockEvent<String> buildTodoSyncSuccess(String value){
        return new ClockEvent<>(TODO_SYNC_SUCCESS,value);
    }

    public static ClockEvent<Boolean> buildAlterTodo(boolean value) {
        return new ClockEvent<>(TODO_ALTER,value);
    }
}
