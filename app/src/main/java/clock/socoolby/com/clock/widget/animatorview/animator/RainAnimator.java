package clock.socoolby.com.clock.widget.animatorview.animator;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;

import clock.socoolby.com.clock.widget.animatorview.AbstractAnimator;
import clock.socoolby.com.clock.widget.animatorview.I_AnimatorEntry;


//原作者：https://github.com/xianfeng99/Particle

public class RainAnimator extends AbstractAnimator<RainAnimator.Rain> {
    public static final String NAME="Rain";

    public RainAnimator(int entryQuantity) {
        super(entryQuantity);
    }

    public RainAnimator() {
        super(50);
    }

    @Override
    public Rain createNewEntry() {
        return new Rain();
    }

    private	final int size = 50;	//长度在0-50像素

    public class Rain implements I_AnimatorEntry {
        private Rect point;		//雨点
        private Point speed;		//雨点x,y方向速度
        private int rainColor;

        public Rain(){
            point = new Rect();
            speed = new Point();
            reset();
        }

        private void reset(){
            int x = rand.nextInt(width);
            int y = rand.nextInt(height);
            int w = rand.nextInt(size / 2);
            int h = rand.nextInt(size);

            w = w > h ? h : w;

            point.left = x;
            point.top = y;
            point.right = x - w;
            point.bottom = y + h;

//		int speedX = rand.nextInt(size / 2);
//		int speedY = rand.nextInt(size);
            int speedX = w;
            int speedY = h;

            speedX = speedX == 0  ? 1 : speedX;
            speedY = speedY == 0  ? 1 : speedY;
            speedX = speedX > speedY ? speedY : speedX;

            speed.x = -speedX;
            speed.y = speedY;
            if(randColor)
                randomColorIfAble();
            this.rainColor=color;
        }

        public void printPosition(){
            Log.d("rainPoint", "percent : " + point.left + " y : " + point.top + " r : " + point.right + " b : " + point.bottom);
        }

        @Override
        public void move(int maxWidth, int maxHight) {
            point.left += speed.x;
            point.top += speed.y;
            point.right = point.right + speed.x;
            point.bottom = point.bottom + speed.y;

            if(point.left < 0 || point.left > width || point.bottom > height){
                reset();
            }
            speed.y += rand.nextBoolean() ? 1 : 0;
        }

        @Override
        public void onDraw(Canvas canvas, Paint mPaint) {
            mPaint.setColor(rainColor);
            canvas.drawLine(point.left, point.top, point.right, point.bottom, mPaint);
        }

        @Override
        public void setAnimatorEntryColor(int color) {
           this.rainColor=color;
        }
    }
}
