package clock.socoolby.com.clock.widget.animatorview;

public interface I_ActionAbleObject extends I_Matrix{
    void setTrajectory(I_Trajectory trajectory);

    void setSpeedInterpolator(I_SpeedInterpolator speedInterpolator);

    void matrix(I_Matrix matrix);

    //生成
    void show();

    //销毁
    void destroy();

}
