package clock.socoolby.com.clock.todo.microsoft.bean.base;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Date;

import clock.socoolby.com.clock.net.base.I_JsonObject;
import clock.socoolby.com.clock.todo.microsoft.utils.TypeUtils;

/**
 *
 * https://docs.microsoft.com/zh-cn/graph/api/resources/recurrencerange?view=graph-rest-beta
 *
 * endDate	Date	定期模式的停止应用日期。 会议的最后一次发生时间可能不是此日期，具体视事件的定期模式而定。 如果 type 为 endDate，此为必需属性。
 * numberOfOccurrences	Int32	事件重复发生次数。 如果 type 为 numbered，此为必需属性，且必须为正数。
 * recurrenceTimeZone	String	startDate 和 endDate 属性的时区。 此为可选属性。 如果未指定，使用的是事件时区。
 * startDate	日期	定期模式的开始应用日期。 会议的第一次发生时间可能是此日期，也可能晚于此日期，具体视事件的定期模式而定。 必须与定期事件的 start 属性值相同。 此为必需属性。
 * type	String	定期范围。 可取值为：endDate、noEnd、numbered。 此为必需属性。
 * type 属性可用于指定不同类型的 recurrenceRange。 请注意每种类型的必需属性，如下表所述。
 * *
 */
public class RecurrenceRange implements I_JsonObject {
    Date endDate ;
    Integer numberOfOccurrences;
    String recurrenceTimeZone;
    Date startDate;
    RecurrenceRangeTypeEnum type;

    @Override
    public void fromJson(JSONObject jsonObject) throws JSONException {
        type=RecurrenceRangeTypeEnum.valueOf(jsonObject.getString("type"));
        switch (type){
            case endDate:
                try {
                    endDate= TypeUtils.deserializeNotTime(jsonObject.getString("endDate"));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                break;
            case noEnd:
                break;
            case numbered:
                numberOfOccurrences=jsonObject.getInt("numberOfOccurrences");
        }
        recurrenceTimeZone=jsonObject.getString("recurrenceTimeZone");
        try {
            startDate=TypeUtils.deserializeNotTime(jsonObject.getString("startDate"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void toJson(JSONObject jsonObject) throws JSONException {

    }
}
