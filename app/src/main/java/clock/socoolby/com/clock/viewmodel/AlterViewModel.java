package clock.socoolby.com.clock.viewmodel;

import android.net.Uri;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import clock.socoolby.com.clock.model.SharePerferenceModel;

public class AlterViewModel extends ViewModel {


    private MutableLiveData<Uri> handUpMusic=new MutableLiveData<>();

    private MutableLiveData<Integer> handUPDialy=new MutableLiveData<>();

    private MutableLiveData<String> handUpMusicName=new MutableLiveData<>();

    //alter handUp UI

    SharePerferenceModel model;

    public AlterViewModel(SharePerferenceModel model) {
        this.model = model;
        loadFromModel();
    }

    public void loadFromModel(){
        String handUpMusicString=model.getHandUpMusic();
        Uri musicUri=handUpMusicString.isEmpty()?null:Uri.parse(handUpMusicString);

        handUpMusic.setValue(musicUri);
        handUpMusicName.setValue(model.getHandUpMusicName());

        handUPDialy.setValue(model.getHandUpDialy());
    }

    public MutableLiveData<Uri> getHandUpMusic() {
        return handUpMusic;
    }

    public MutableLiveData<Integer> getHandUPDialy() {
        return handUPDialy;
    }

    public void setHandUpMusic(Uri handUpMusic) {
        this.handUpMusic.setValue(handUpMusic);
        if(handUpMusic==null)
            model.setHandUpMusic("");
        else
            model.setHandUpMusic(handUpMusic.toString());
    }

    public void setHandUPDialy(Integer handUPDialy) {
        this.handUPDialy.setValue(handUPDialy);
        model.setHandUpDialy(handUPDialy);
    }

    public MutableLiveData<String> getHandUpMusicName() {
        return handUpMusicName;
    }

    public void setHandUpMusicName(String handUpMusicName) {
        this.handUpMusicName.setValue(handUpMusicName);
        model.setHandUpMusicName(handUpMusicName);
    }
}
