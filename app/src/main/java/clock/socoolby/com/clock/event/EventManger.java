package clock.socoolby.com.clock.event;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.jeremyliao.liveeventbus.LiveEventBus;

import clock.socoolby.com.clock.model.DateModel;


/**
 * 因为使用的是livedata,所以尽量使用状态相关的，
 * 参考：https://www.jianshu.com/p/b2646bf112ce
 */
public class EventManger {

    public static final String TAG=EventManger.class.getName();

    private static LiveEventBus liveEventBus;

    public static void init(){
        LiveEventBus.get()
                .config()
                //.supportBroadcast(this)
                .lifecycleObserverAlwaysActive(false)
                .autoClear(false);
        liveEventBus=LiveEventBus.get();
    }

    public static <T> void post(@NonNull ClockEvent<T> event) {
        if(liveEventBus==null)
            init();
        liveEventBus.with(event.getEventType(),(Class<T>)event.getValue().getClass()).post(event.getValue());
    }

    public static <T> void addClockEventListener(@NonNull LifecycleOwner owner, @NonNull String eventType, @NonNull Class<T> valueClass, @NonNull ClockEventListener<T> listener){
        if(liveEventBus==null)
            init();
        liveEventBus.with(eventType,valueClass).observe(owner,t-> listener.onEvent(ClockEvent.build(eventType,t)));
    }


    //调用此函数得手动调用删除，没办法，先用
    public static <T> EventListenerHandle<T> addClockEventListenerWithForever(@NonNull String eventType, @NonNull Class<T> valueClass, @NonNull ClockEventListener<T> listener){
        if(liveEventBus==null)
            init();
        Observer<T> handle= new Observer<T>() {
            @Override
            public void onChanged(T t) {
                listener.onEvent(ClockEvent.build(eventType,t));
            }
        };
        liveEventBus.with(eventType,valueClass).observeForever(handle);

        return new EventListenerHandle<>(eventType,valueClass,handle);
    }

    public static void removeClockEventListener(EventListenerHandle handle){
        if(liveEventBus==null)
            init();
        timber.log.Timber.d("removeClockEventListener: "+handle.getEventType());
        liveEventBus.with(handle.getEventType(),handle.getValueClass()).removeObserver(handle.getHandle());
    }

    public static void addHandupListener(@NonNull LifecycleOwner owner, @NonNull ClockEventListener<Boolean> listener){
         addClockEventListener(owner,ClockEvent.HANDUP,Boolean.class,listener);
    }

    public static void addAlterListener(@NonNull LifecycleOwner owner, @NonNull ClockEventListener<Boolean> listener){
        addClockEventListener(owner,ClockEvent.TODO_ALTER,Boolean.class,listener);
    }


    public static void addHourAnimatorListener(@NonNull LifecycleOwner owner, @NonNull ClockEventListener<Boolean> listener){
         addClockEventListener(owner,ClockEvent.HOUR_ANIMATOR,Boolean.class,listener);
    }

    public static void addHeartbeatListener(@NonNull LifecycleOwner owner, @NonNull ClockEventListener<Boolean> listener){
         addClockEventListener(owner,ClockEvent.HEARTBEAT,Boolean.class,listener);
    }

    public static void addLongSleepWakeUpListener(@NonNull LifecycleOwner owner, @NonNull ClockEventListener<Integer> listener){
        addClockEventListener(owner,ClockEvent.LONG_SLEEP_WAKE_UP,Integer.class,listener);
    }

    public static EventListenerHandle addHeartbeatListener(@NonNull ClockEventListener<Boolean> listener){
        return addClockEventListenerWithForever(ClockEvent.HEARTBEAT,Boolean.class,listener);
    }

    public static void addCountingStateChangedListener(@NonNull LifecycleOwner owner, @NonNull ClockEventListener<Boolean> listener){
        addClockEventListener(owner,ClockEvent.COUNTING_STATE_CHANGED,Boolean.class,listener);
    }

    public static void addNewDayListener(@NonNull LifecycleOwner owner, @NonNull ClockEventListener<DateModel> listener){
        addClockEventListener(owner,ClockEvent.NEW_DAY,DateModel.class,listener);
    }

    public static void addToduSyncSeccuss(@NonNull LifecycleOwner owner, @NonNull ClockEventListener<String> listener){
        addClockEventListener(owner,ClockEvent.TODO_SYNC_SUCCESS,String.class,listener);
    }
}


