package clock.socoolby.com.clock.net.auth;

public interface AuthCallback {
    void onSuccess();

    void onError(final Exception exception);

    /**
     * Will be called if user cancels the flow.
     */
    void onCancel();
}
