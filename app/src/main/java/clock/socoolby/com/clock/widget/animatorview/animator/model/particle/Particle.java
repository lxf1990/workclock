package clock.socoolby.com.clock.widget.animatorview.animator.model.particle;

import android.graphics.Canvas;
import android.graphics.Paint;

import clock.socoolby.com.clock.widget.animatorview.I_AnimatorEntry;

public class Particle implements I_AnimatorEntry {
    //运动相关
    public float aX;//加速度
    public float aY;//加速度Y
    public float vX;//速度X
    public float vY;//速度Y
    public long born;//诞生时间

    //绘制相关
    public float x;//点位X,统一为中心点X
    public float y;//点位Y，统一为中心点Y
    public int color;//颜色
    public float r;//半径,主要用于计算所占面积，不只用于圆
    public int alpha;//透明度
    public I_PraticleDraw style;

    public Particle clone() {
        Particle clone = null;
        try {
            clone = (Particle) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return clone;
    }

    @Override
    public void move(int maxWidth, int maxHight) {

    }

    @Override
    public void onDraw(Canvas canvas, Paint mPaint) {
        mPaint.setColor(color);
        if(style!=null)
             style.draw(canvas,this,mPaint);
    }

    @Override
    public void setAnimatorEntryColor(int color) {
       this.color=color;
    }
}
