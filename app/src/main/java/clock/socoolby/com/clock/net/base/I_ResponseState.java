package clock.socoolby.com.clock.net.base;

public interface I_ResponseState extends I_JsonObject {
    boolean hasNext();

    String nextLinkUrl();

    String getServiceName();
}
